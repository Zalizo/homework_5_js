/*
1. Це функція яка належить об'єкту. Внутрішні об'єкти є string та array.
2. Властивість є набір значень, який є членом об'єкта і може містити будь який тип даних.
3. Він використовується з метою передачі інформації від крапки та від дужок ().
*/
function createNewUser() {
  let firstName = prompt("Введіть ваше ім'я:");
  let lastName = prompt("Введіть ваше прізвище:");

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin: function () {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    setFirstName: function (newFirstName) {
      this.firstName = newFirstName;
    },
    setLastName: function (newLastName) {
      this.lastName = newLastName;
    },
  };

  return newUser;
}

const user = createNewUser();
console.log(user.getLogin());

user.setFirstName("Нове ім'я");
user.setLastName("Нове прізвище");
console.log(user.getLogin());
